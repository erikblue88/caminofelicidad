﻿using System.Collections.Generic;

[System.Serializable]
public class Respuesta
{
    public string respuesta;
    public bool correcta;
}

[System.Serializable]
public class Pregunta
{
    public string pregunta;
    public string animo;
    public string color;
    public List<Respuesta> respuestas;
}

[System.Serializable]
public class Root
{
    public List<Pregunta> preguntas;
}
