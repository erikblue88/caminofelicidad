﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour
{
    public List<AudioClip> fxAudios;
    public AudioSource fxSource;

    public void PlayAudio(int indexAudio)
    {
        fxSource.clip = fxAudios[indexAudio];
        fxSource.Play();
    }
}
