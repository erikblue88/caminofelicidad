﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public TextAsset preguntasDoc;
    public Text pregunta;
    public List<Text> respuestas;
    public List<Transform> casillas;
    public List<Material> coloresAnimo;
    public GameObject personaje;
    public GameObject panelCorrecto;
    public GameObject panelIncorrecto;
    public GameObject panelGanaste;
    public GameObject panelPorras;
    public GameObject panelInstructions;
    public GameObject estrellitaCorrecta;
    public Text porrasText;
    public float smoothing = 5f;
    public Animator feliz;
    public Animator enojado;
    public Animator triste;
    public Animator temeroso;
    public Image termometro;
    public AudioControl ac;

    Root preguntas;
    List<Pregunta> preguntasMiedo = new List<Pregunta>();
    List<Pregunta> preguntasTristeza = new List<Pregunta>();
    List<Pregunta> preguntasFuria = new List<Pregunta>();
    List<Pregunta> preguntasAlegria = new List<Pregunta>();
    bool pasopor5, pasopor10, pasopor15, pasopor20;
    public int aciertos; //Los aciertos siempre son igual al número de casillas avanzadas hasta el momento

    // Start is called before the first frame update
    void Start()
    {
        preguntas = JsonUtility.FromJson<Root>(preguntasDoc.text);
        RellenaListas();
        StartCoroutine(PintaPregunta());
    }

    void RellenaListas()
    {
        preguntasMiedo = new List<Pregunta>();
        preguntasTristeza = new List<Pregunta>();
        preguntasFuria = new List<Pregunta>();
        preguntasAlegria = new List<Pregunta>();
        preguntas = JsonUtility.FromJson<Root>(preguntasDoc.text);
        foreach (Pregunta pregunta in preguntas.preguntas)
        {
            switch (pregunta.animo)
            {
                case "ENOJADO":
                    preguntasFuria.Add(pregunta);
                    break;
                case "TRISTE":
                    preguntasTristeza.Add(pregunta);
                    break;
                case "MIEDO":
                    preguntasMiedo.Add(pregunta);
                    break;
                case "FELIZ":
                    preguntasAlegria.Add(pregunta);
                    break;
            }
            Debug.Log("Found pregunta: " + pregunta.animo + " " + pregunta.color);
        }
    }

    IEnumerator PintaPregunta()
    {
        if (aciertos < 5)
        {
            int idxPregunta = Random.Range(0, preguntasMiedo.Count);
            pregunta.text = preguntasMiedo[idxPregunta].pregunta;
            List<Respuesta> curRespuestas = preguntasMiedo[idxPregunta].respuestas;
            for(int x=0; x<3; x++)
            {
                int idxRespuesta = Random.Range(0, curRespuestas.Count);
                respuestas[x].text = curRespuestas[idxRespuesta].respuesta;
                respuestas[x].transform.parent.GetComponent<Answer>().correcta = curRespuestas[idxRespuesta].correcta;
                curRespuestas.RemoveAt(idxRespuesta);
            }
            preguntasMiedo.RemoveAt(idxPregunta);
            if(preguntasMiedo.Count == 0)
                RellenaListas();
            yield break;
        }
        if(aciertos == 5 && !pasopor5) 
        {
            pasopor5 = true;
            ac.PlayAudio(2);
            porrasText.text = "¡¡Vamos!! ¡¡Lo estás haciendo muy bien!!";
            personaje.SetActive(false);
            panelPorras.SetActive(true);
            yield return new WaitForSeconds(3);
            panelPorras.SetActive(false);
            personaje.SetActive(true);
            temeroso.Play("FadeOut");
            triste.Play("FadeIn");
        }
        if (aciertos < 10)
        {
            int idxPregunta = Random.Range(0, preguntasTristeza.Count);
            pregunta.text = preguntasTristeza[idxPregunta].pregunta;
            List<Respuesta> curRespuestas = preguntasTristeza[idxPregunta].respuestas;
            for (int x = 0; x < 3; x++)
            {
                int idxRespuesta = Random.Range(0, curRespuestas.Count);
                respuestas[x].text = curRespuestas[idxRespuesta].respuesta;
                respuestas[x].transform.parent.GetComponent<Answer>().correcta = curRespuestas[idxRespuesta].correcta;
                curRespuestas.RemoveAt(idxRespuesta);
            }
            preguntasTristeza.RemoveAt(idxPregunta);
            if (preguntasTristeza.Count == 0)
                RellenaListas();
            yield break;
        }
        if (aciertos == 10 && !pasopor10)
        {
            pasopor10 = true;
            ac.PlayAudio(2);
            porrasText.text = "¡¡Así se hace!! ¡¡Ya casi lo logras!!";
            personaje.SetActive(false);
            panelPorras.SetActive(true);
            yield return new WaitForSeconds(3);
            panelPorras.SetActive(false);
            personaje.SetActive(true);
            triste.Play("FadeOut");
            enojado.Play("FadeIn");
        }
        if (aciertos < 15)
        {
            int idxPregunta = Random.Range(0, preguntasFuria.Count);
            pregunta.text = preguntasFuria[idxPregunta].pregunta;
            List<Respuesta> curRespuestas = preguntasFuria[idxPregunta].respuestas;
            for (int x = 0; x < 3; x++)
            {
                int idxRespuesta = Random.Range(0, curRespuestas.Count);
                respuestas[x].text = curRespuestas[idxRespuesta].respuesta;
                respuestas[x].transform.parent.GetComponent<Answer>().correcta = curRespuestas[idxRespuesta].correcta;
                curRespuestas.RemoveAt(idxRespuesta);
            }
            preguntasFuria.RemoveAt(idxPregunta);
            if (preguntasFuria.Count == 0)
                RellenaListas();
            yield break;
        }
        if (aciertos == 15 && !pasopor15)
        {
            pasopor15 = true;
            ac.PlayAudio(2);
            porrasText.text = "¡¡Eres el mejor!! ¡¡Directo a la victoria!!";
            personaje.SetActive(false);
            panelPorras.SetActive(true);
            yield return new WaitForSeconds(3);
            panelPorras.SetActive(false);
            personaje.SetActive(true);
            enojado.Play("FadeOut");
            feliz.Play("FadeIn");
        }
        if (aciertos < 20)
        {
            int idxPregunta = Random.Range(0, preguntasAlegria.Count);
            pregunta.text = preguntasAlegria[idxPregunta].pregunta;
            List<Respuesta> curRespuestas = preguntasAlegria[idxPregunta].respuestas;
            for (int x = 0; x < 3; x++)
            {
                int idxRespuesta = Random.Range(0, curRespuestas.Count);
                respuestas[x].text = curRespuestas[idxRespuesta].respuesta;
                respuestas[x].transform.parent.GetComponent<Answer>().correcta = curRespuestas[idxRespuesta].correcta;
                curRespuestas.RemoveAt(idxRespuesta);
            }
            preguntasAlegria.RemoveAt(idxPregunta);
            if (preguntasAlegria.Count == 0)
                RellenaListas();
            yield break;
        }
    }

    IEnumerator Porras()
    {
        yield return new WaitForSeconds(3);

    }

    IEnumerator correcta(Answer answer)
    {
        ac.PlayAudio(0);
        aciertos++;
        termometro.fillAmount += 0.05f;
        switch (aciertos) //Aquí van las animaciones de cambio de ánimo
        {
            case 1:
                personaje.GetComponent<Renderer>().material = coloresAnimo[0];
                break;
            case 5:
                personaje.GetComponent<Renderer>().material = coloresAnimo[1];
                break;
            case 10:
                personaje.GetComponent<Renderer>().material = coloresAnimo[2];
                break;
            case 15:
                personaje.GetComponent<Renderer>().material = coloresAnimo[3];
                break;
        }
        panelCorrecto.SetActive(true);
        answer.spritecorrecta.SetActive(true);
        estrellitaCorrecta.SetActive(true);
        while (Vector3.Distance(personaje.transform.position, casillas[aciertos-1].position) > 0.01f)
        {
            personaje.transform.position = Vector3.Lerp(personaje.transform.position, casillas[aciertos - 1].position, smoothing * Time.deltaTime);
            yield return null;
        }
        yield return new WaitForSeconds(1);
        if(aciertos < 20)
        {
            StartCoroutine(PintaPregunta());
            panelCorrecto.SetActive(false);
            answer.spritecorrecta.SetActive(false);
        }
        else
        {
            ac.PlayAudio(2);
            //Ganaste!!
            panelGanaste.SetActive(true);
            print("GANASTE!!!");
        }
        estrellitaCorrecta.SetActive(false);
    }

    IEnumerator incorrecta(Answer answer)
    {
        ac.PlayAudio(1);
        panelIncorrecto.SetActive(true);
        answer.spriteincorrecta.SetActive(true);
        yield return new WaitForSeconds(2);
        StartCoroutine(PintaPregunta());
        panelIncorrecto.SetActive(false);
        answer.spriteincorrecta.SetActive(false);
    }

    #region public methods
    public void RevisaRespuesta(Answer answer) //Se llama en los botones de las respuestas
    {
        if (answer.correcta)
        {
            StartCoroutine(correcta(answer));
        }
        else
        {
            StartCoroutine(incorrecta(answer));
        }
            
    }

    public void Reiniciar()
    {
        SceneManager.LoadScene(0);
    }

    public void HideInstructions()
    {
        panelInstructions.SetActive(false);
        personaje.SetActive(true);
    }
    #endregion //public methods
}
